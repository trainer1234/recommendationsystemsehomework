﻿using MyMediaLite.Data;
using MyMediaLite.Eval;
using MyMediaLite.IO;
using MyMediaLite.RatingPrediction;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace ConsoleApplication1
{
    public class RecommendHandler
    {
        public static String PredictOnSQL()
        {
            String resultsPredict = "";

            string queryString = @"SELECT * FROM View_AmazoneBooksRating;";
            SqlConnection connection = new SqlConnection(SqlHandler.strCon);
            SqlCommand command1 = new SqlCommand(queryString, connection);
            SqlCommand command2 = new SqlCommand(queryString, connection);
            connection.Open();
            SqlDataReader reader1 = command1.ExecuteReader();


            Mapping m1 = new Mapping();
            Mapping m2 = new Mapping();
            Mapping m3 = new Mapping();
            Mapping m4 = new Mapping();
            // load the data
            var training_data = RatingData.Read((IDataReader)reader1, m1, m2);
            reader1.Close();
            SqlDataReader reader2 = command2.ExecuteReader();
            var test_data = RatingData.Read((IDataReader)reader2, m3, m4);
            reader2.Close();

            // set up the recommender
            var recommender = new UserItemBaseline();
            recommender.Ratings = training_data;
            recommender.Train();

            // measure the accuracy on the test data set
            var results = recommender.Evaluate(test_data);
            resultsPredict += "RMSE=" + results["RMSE"] + "   MAE=" + results["MAE"] + Environment.NewLine;

            // make a prediction for a certain user and item
            resultsPredict += "Sample predict of userid 1 with item 0829814000 = " + recommender.Predict(1, 0829814000) + Environment.NewLine;
            resultsPredict += "Sample predict of userid 2 with item 1882931173 = " + recommender.Predict(2, 1882931173) + Environment.NewLine;
            resultsPredict += "Sample predict of userid 3 with item 0826414346 = " + recommender.Predict(3, 0826414346) + Environment.NewLine;

            var bmf = new BiasedMatrixFactorization { Ratings = training_data };
            //Console.WriteLine(bmf.DoCrossValidation());
            //Console.ReadKey();

            //// load the data
            //training_data = ItemData.Read("ratings.csv", null, null, true);
            //test_data = ItemData.Read("ratings.csv", null, null, true);

            //// set up the recommender
            //recommender = new MostPopular();
            //recommender.Feedback = training_data;
            //recommender.Train();

            // measure the accuracy on the test data set
            results = recommender.Evaluate(test_data, training_data);
            //foreach (var key in results.Keys)
            //    Console.WriteLine("{0}={1}", key, results[key]);
            //Console.WriteLine(results);

            // make a score prediction for a certain user and item
            //Console.WriteLine(recommender.Predict(1, 1));
            connection.Close();

            return resultsPredict;
        }

        public static String PredictOn1MSQL()
        {
            String resultsPredict = "";

            string queryString = @"SELECT * FROM View_AmazoneBooksRating1M;";
            SqlConnection connection = new SqlConnection(SqlHandler.strCon);
            SqlCommand command1 = new SqlCommand(queryString, connection);
            SqlCommand command2 = new SqlCommand(queryString, connection);
            connection.Open();
            SqlDataReader reader1 = command1.ExecuteReader();


            Mapping m1 = new Mapping();
            Mapping m2 = new Mapping();
            Mapping m3 = new Mapping();
            Mapping m4 = new Mapping();
            // load the data
            var training_data = RatingData.Read((IDataReader)reader1, m1, m2);
            reader1.Close();
            SqlDataReader reader2 = command2.ExecuteReader();
            var test_data = RatingData.Read((IDataReader)reader2, m3, m4);

            // set up the recommender
            var recommender = new UserItemBaseline();
            recommender.Ratings = training_data;
            recommender.Train();

            // measure the accuracy on the test data set
            var results = recommender.Evaluate(test_data);
            //Console.WriteLine("RMSE={0} MAE={1}", results["RMSE"], results["MAE"]);
            resultsPredict += "RMSE=" + results["RMSE"] + "   MAE=" + results["MAE"] + System.Environment.NewLine;
            //Console.WriteLine(results);

            // make a prediction for a certain user and item
            //Console.WriteLine("Sample predict of userid 2 with item 0595344550 = ", recommender.Predict(1, 0595344550));
            resultsPredict += "Sample predict of userid 1 with item 0829814000 = " + recommender.Predict(1, 0829814000) + System.Environment.NewLine;
            resultsPredict += "Sample predict of userid 2 with item 1882931173 = " + recommender.Predict(2, 1882931173) + System.Environment.NewLine;
            resultsPredict += "Sample predict of userid 3 with item 0826414346 = " + recommender.Predict(3, 0826414346) + System.Environment.NewLine;

            var bmf = new BiasedMatrixFactorization { Ratings = training_data };
            //Console.WriteLine(bmf.DoCrossValidation());
            //Console.ReadKey();

            //// load the data
            //training_data = ItemData.Read("ratings.csv", null, null, true);
            //test_data = ItemData.Read("ratings.csv", null, null, true);

            //// set up the recommender
            //recommender = new MostPopular();
            //recommender.Feedback = training_data;
            //recommender.Train();

            // measure the accuracy on the test data set
            results = recommender.Evaluate(test_data, training_data);
            //foreach (var key in results.Keys)
            //    Console.WriteLine("{0}={1}", key, results[key]);
            //Console.WriteLine(results);

            // make a score prediction for a certain user and item
            //Console.WriteLine(recommender.Predict(1, 1));

            return resultsPredict;
        }

        static void PredictOnText()
        {
            // load the data
            var training_data = RatingData.Read("ratings.csv", null, null, true);
            var test_data = RatingData.Read("RatingsTest.csv", null, null, true);

            // set up the recommender
            var recommender = new UserItemBaseline();
            recommender.Ratings = training_data;
            recommender.Train();

            // measure the accuracy on the test data set
            var results = recommender.Evaluate(test_data);
            //Console.WriteLine("RMSE={0} MAE={1}", results["RMSE"], results["MAE"]);
            Console.WriteLine(results);

            // make a prediction for a certain user and item
            Console.WriteLine("Sample predict of userid 2 with item 1673 = {0}", recommender.Predict(2, 1673));

            var bmf = new BiasedMatrixFactorization { Ratings = training_data };
            //Console.WriteLine(bmf.DoCrossValidation());
            Console.ReadKey();

            //// load the data
            //training_data = ItemData.Read("ratings.csv", null, null, true);
            //test_data = ItemData.Read("ratings.csv", null, null, true);

            //// set up the recommender
            //recommender = new MostPopular();
            //recommender.Feedback = training_data;
            //recommender.Train();

            // measure the accuracy on the test data set
            results = recommender.Evaluate(test_data, training_data);
            foreach (var key in results.Keys)
                Console.WriteLine("{0}={1}", key, results[key]);
            Console.WriteLine(results);

            // make a score prediction for a certain user and item
            Console.WriteLine(recommender.Predict(1, 1));
        }

        static void Amazone2SQL(string tableName, string filePath)
        {
            //porting txt into sql
            SqlConnection conn = new SqlConnection(SqlHandler.strCon);
            string sql = "INSERT INTO " + tableName + " (ProductID,Title,Price,UserId,ProfileName,Helpfulness,Score,Time,Summary,Text) VALUES (@ProductID,@Title,@Price,@UserId,@ProfileName,@Helpfulness,@Score,@Time,@Summary,@Text)";
            //string filePath = @"D:\Books.txt";
            try
            {
                conn.Open();
                StreamReader sr = new StreamReader(filePath);

                String[] line = new String[11];
                int numError = 0;
                for (int i = 0; i < 1000000; i++)
                {
                    try
                    {
                        for (int j = 0; j < 11; j++)
                        {
                            line[j] = sr.ReadLine();
                            if(j == 5 || j == 6)
                            {
                                String splitLine = line[j].ToString().Split(':')[1];
                                Double tmp = Convert.ToDouble(splitLine.Split('/')[0]) / Convert.ToDouble(splitLine.Split('/')[1]);
                                if (Double.IsNaN(tmp))
                                    tmp = 0.0;
                                line[j] = tmp.ToString();
                            }
                        }

                        SqlCommand cmd = new SqlCommand(sql, conn);

                        cmd.Parameters.AddWithValue("@ProductID", line[0].ToString().Split(':')[1].Trim());
                        cmd.Parameters.AddWithValue("@Title", line[1].ToString().Split(':')[1].Trim());
                        cmd.Parameters.AddWithValue("@Price", line[2].ToString().Split(':')[1].Trim());
                        cmd.Parameters.AddWithValue("@UserId", line[3].ToString().Split(':')[1].Trim());
                        cmd.Parameters.AddWithValue("@ProfileName", line[4].ToString().Split(':')[1].Trim());
                        cmd.Parameters.AddWithValue("@Helpfulness", line[5].ToString());
                        cmd.Parameters.AddWithValue("@Score", line[6].ToString());
                        cmd.Parameters.AddWithValue("@Time", line[7].ToString().Split(':')[1].Trim());
                        cmd.Parameters.AddWithValue("@Summary", line[8].ToString().Split(':')[1].Trim());
                        cmd.Parameters.AddWithValue("@Text", line[9].ToString().Split(':')[1].Trim());

                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        numError++;
                        Console.WriteLine("Error on {0} record.", i);
                    }
                } //end for
                Console.WriteLine("Total Error on {0} total records.", numError);
            }
            catch (SqlException ex)
            {
                string msg = "Insert Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                conn.Close();
            }
        }

        static void ReadBigTextFile(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                StreamReader sr = new StreamReader(filePath);

                String[] line = new String[11];
                for (int i = 0; i < 4; i++)
                {
                    for(int j = 0; j < 11; j++)
                    {
                        line[j] = sr.ReadLine();
                        if(j == 5 || j == 6)
                        {
                            string splitLine = line[j].ToString().Split(':')[1];
                            Double tmp = Convert.ToDouble(splitLine.Split('/')[0]) / Convert.ToDouble(splitLine.Split('/')[1]);
                            line[j] = tmp.ToString();
                        }
                        Console.WriteLine("[Line {0}: {1}", j, line[j]);
                    }
                }
            }

            Console.ReadKey();
        }
    }
}
