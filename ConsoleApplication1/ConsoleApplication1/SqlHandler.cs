﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ConsoleApplication1
{
    public class SqlHandler
    {
        public static string strCon = ConfigurationManager.ConnectionStrings["Amazone"].ConnectionString;

        public DataTable Query(string connectionString, string query)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            // this will query your database and return the result to your datatable
            da.Fill(dataTable);

            conn.Close();
            da.Dispose();

            return dataTable;
        }
    }
}
