﻿namespace ConsoleApplication1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label productIdLabel;
            System.Windows.Forms.Label titleLabel;
            System.Windows.Forms.Label priceLabel;
            System.Windows.Forms.Label userIdLabel;
            System.Windows.Forms.Label profileNameLabel;
            System.Windows.Forms.Label helpfulnessLabel;
            System.Windows.Forms.Label summaryLabel;
            System.Windows.Forms.Label textLabel;
            System.Windows.Forms.Label timeLabel1;
            System.Windows.Forms.Label scoreLabel;
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode8 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode9 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode10 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode11 = new DevExpress.XtraGrid.GridLevelNode();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.booksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moviesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ratingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.booksUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moviesUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commonUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.experiementalResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traditionalMethodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traditionalMethodFor1MToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traditionalMethodForFilm100KToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traditionalMethodForFilm1MToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageData = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageBindingData = new DevExpress.XtraTab.XtraTabPage();
            this.scoreSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.amazoneBooksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.amazoneDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.amazoneDataSet = new ConsoleApplication1.AmazoneDataSet();
            this.timeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.productIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.titleTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.priceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.userIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.profileNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.helpfulnessSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.summaryTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.textRichTextBox = new System.Windows.Forms.RichTextBox();
            this.xtraTabPageAbout = new DevExpress.XtraTab.XtraTabPage();
            this.richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            this.amazoneBooksTableAdapter = new ConsoleApplication1.AmazoneDataSetTableAdapters.AmazoneBooksTableAdapter();
            this.tableAdapterManager = new ConsoleApplication1.AmazoneDataSetTableAdapters.TableAdapterManager();
            this.amazoneMoviesTVBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.amazoneMoviesTVTableAdapter = new ConsoleApplication1.AmazoneDataSetTableAdapters.AmazoneMoviesTVTableAdapter();
            this.viewAmazoneBooksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.view_AmazoneBooksTableAdapter = new ConsoleApplication1.AmazoneDataSetTableAdapters.View_AmazoneBooksTableAdapter();
            this.viewAmazoneBooksRatingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.view_AmazoneBooksRatingTableAdapter = new ConsoleApplication1.AmazoneDataSetTableAdapters.View_AmazoneBooksRatingTableAdapter();
            this.viewAmazoneMoviesTVBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.view_AmazoneMoviesTVTableAdapter = new ConsoleApplication1.AmazoneDataSetTableAdapters.View_AmazoneMoviesTVTableAdapter();
            this.viewBooksAndMoviesCommonBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.view_BooksAndMoviesCommonTableAdapter = new ConsoleApplication1.AmazoneDataSetTableAdapters.View_BooksAndMoviesCommonTableAdapter();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            productIdLabel = new System.Windows.Forms.Label();
            titleLabel = new System.Windows.Forms.Label();
            priceLabel = new System.Windows.Forms.Label();
            userIdLabel = new System.Windows.Forms.Label();
            profileNameLabel = new System.Windows.Forms.Label();
            helpfulnessLabel = new System.Windows.Forms.Label();
            summaryLabel = new System.Windows.Forms.Label();
            textLabel = new System.Windows.Forms.Label();
            timeLabel1 = new System.Windows.Forms.Label();
            scoreLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageData.SuspendLayout();
            this.xtraTabPageBindingData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scoreSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amazoneBooksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amazoneDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amazoneDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpfulnessSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryTextEdit.Properties)).BeginInit();
            this.xtraTabPageAbout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.amazoneMoviesTVBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewAmazoneBooksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewAmazoneBooksRatingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewAmazoneMoviesTVBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewBooksAndMoviesCommonBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // productIdLabel
            // 
            productIdLabel.AutoSize = true;
            productIdLabel.Location = new System.Drawing.Point(45, 19);
            productIdLabel.Name = "productIdLabel";
            productIdLabel.Size = new System.Drawing.Size(61, 13);
            productIdLabel.TabIndex = 0;
            productIdLabel.Text = "Product Id:";
            // 
            // titleLabel
            // 
            titleLabel.AutoSize = true;
            titleLabel.Location = new System.Drawing.Point(45, 45);
            titleLabel.Name = "titleLabel";
            titleLabel.Size = new System.Drawing.Size(31, 13);
            titleLabel.TabIndex = 2;
            titleLabel.Text = "Title:";
            // 
            // priceLabel
            // 
            priceLabel.AutoSize = true;
            priceLabel.Location = new System.Drawing.Point(45, 71);
            priceLabel.Name = "priceLabel";
            priceLabel.Size = new System.Drawing.Size(34, 13);
            priceLabel.TabIndex = 4;
            priceLabel.Text = "Price:";
            // 
            // userIdLabel
            // 
            userIdLabel.AutoSize = true;
            userIdLabel.Location = new System.Drawing.Point(45, 97);
            userIdLabel.Name = "userIdLabel";
            userIdLabel.Size = new System.Drawing.Size(46, 13);
            userIdLabel.TabIndex = 6;
            userIdLabel.Text = "User Id:";
            // 
            // profileNameLabel
            // 
            profileNameLabel.AutoSize = true;
            profileNameLabel.Location = new System.Drawing.Point(376, 19);
            profileNameLabel.Name = "profileNameLabel";
            profileNameLabel.Size = new System.Drawing.Size(71, 13);
            profileNameLabel.TabIndex = 8;
            profileNameLabel.Text = "Profile Name:";
            // 
            // helpfulnessLabel
            // 
            helpfulnessLabel.AutoSize = true;
            helpfulnessLabel.Location = new System.Drawing.Point(376, 45);
            helpfulnessLabel.Name = "helpfulnessLabel";
            helpfulnessLabel.Size = new System.Drawing.Size(66, 13);
            helpfulnessLabel.TabIndex = 10;
            helpfulnessLabel.Text = "Helpfulness:";
            // 
            // summaryLabel
            // 
            summaryLabel.AutoSize = true;
            summaryLabel.Location = new System.Drawing.Point(45, 137);
            summaryLabel.Name = "summaryLabel";
            summaryLabel.Size = new System.Drawing.Size(55, 13);
            summaryLabel.TabIndex = 16;
            summaryLabel.Text = "Summary:";
            // 
            // textLabel
            // 
            textLabel.AutoSize = true;
            textLabel.Location = new System.Drawing.Point(45, 177);
            textLabel.Name = "textLabel";
            textLabel.Size = new System.Drawing.Size(33, 13);
            textLabel.TabIndex = 18;
            textLabel.Text = "Text:";
            // 
            // timeLabel1
            // 
            timeLabel1.AutoSize = true;
            timeLabel1.Location = new System.Drawing.Point(376, 101);
            timeLabel1.Name = "timeLabel1";
            timeLabel1.Size = new System.Drawing.Size(33, 13);
            timeLabel1.TabIndex = 20;
            timeLabel1.Text = "Time:";
            // 
            // scoreLabel
            // 
            scoreLabel.AutoSize = true;
            scoreLabel.Location = new System.Drawing.Point(376, 75);
            scoreLabel.Name = "scoreLabel";
            scoreLabel.Size = new System.Drawing.Size(38, 13);
            scoreLabel.TabIndex = 21;
            scoreLabel.Text = "Score:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.experiementalResultsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(775, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.booksToolStripMenuItem,
            this.moviesToolStripMenuItem,
            this.ratingToolStripMenuItem,
            this.booksUserToolStripMenuItem,
            this.moviesUserToolStripMenuItem,
            this.commonUserToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // booksToolStripMenuItem
            // 
            this.booksToolStripMenuItem.Name = "booksToolStripMenuItem";
            this.booksToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.booksToolStripMenuItem.Text = "Books";
            this.booksToolStripMenuItem.Click += new System.EventHandler(this.booksToolStripMenuItem_Click);
            // 
            // moviesToolStripMenuItem
            // 
            this.moviesToolStripMenuItem.Name = "moviesToolStripMenuItem";
            this.moviesToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.moviesToolStripMenuItem.Text = "Movies";
            this.moviesToolStripMenuItem.Click += new System.EventHandler(this.moviesToolStripMenuItem_Click);
            // 
            // ratingToolStripMenuItem
            // 
            this.ratingToolStripMenuItem.Name = "ratingToolStripMenuItem";
            this.ratingToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.ratingToolStripMenuItem.Text = "Rating";
            this.ratingToolStripMenuItem.Click += new System.EventHandler(this.ratingToolStripMenuItem_Click);
            // 
            // booksUserToolStripMenuItem
            // 
            this.booksUserToolStripMenuItem.Name = "booksUserToolStripMenuItem";
            this.booksUserToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.booksUserToolStripMenuItem.Text = "BooksUser";
            this.booksUserToolStripMenuItem.Click += new System.EventHandler(this.booksUserToolStripMenuItem_Click);
            // 
            // moviesUserToolStripMenuItem
            // 
            this.moviesUserToolStripMenuItem.Name = "moviesUserToolStripMenuItem";
            this.moviesUserToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.moviesUserToolStripMenuItem.Text = "MoviesUser";
            this.moviesUserToolStripMenuItem.Click += new System.EventHandler(this.moviesUserToolStripMenuItem_Click);
            // 
            // commonUserToolStripMenuItem
            // 
            this.commonUserToolStripMenuItem.Name = "commonUserToolStripMenuItem";
            this.commonUserToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.commonUserToolStripMenuItem.Text = "CommonUser";
            this.commonUserToolStripMenuItem.Click += new System.EventHandler(this.commonUserToolStripMenuItem_Click);
            // 
            // experiementalResultsToolStripMenuItem
            // 
            this.experiementalResultsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.traditionalMethodToolStripMenuItem,
            this.traditionalMethodFor1MToolStripMenuItem,
            this.traditionalMethodForFilm100KToolStripMenuItem,
            this.traditionalMethodForFilm1MToolStripMenuItem});
            this.experiementalResultsToolStripMenuItem.Name = "experiementalResultsToolStripMenuItem";
            this.experiementalResultsToolStripMenuItem.Size = new System.Drawing.Size(133, 20);
            this.experiementalResultsToolStripMenuItem.Text = "Experiemental Results";
            // 
            // traditionalMethodToolStripMenuItem
            // 
            this.traditionalMethodToolStripMenuItem.Name = "traditionalMethodToolStripMenuItem";
            this.traditionalMethodToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.traditionalMethodToolStripMenuItem.Text = "Traditional Method For Books 100K";
            this.traditionalMethodToolStripMenuItem.Click += new System.EventHandler(this.traditionalMethodToolStripMenuItem_Click);
            // 
            // traditionalMethodFor1MToolStripMenuItem
            // 
            this.traditionalMethodFor1MToolStripMenuItem.Name = "traditionalMethodFor1MToolStripMenuItem";
            this.traditionalMethodFor1MToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.traditionalMethodFor1MToolStripMenuItem.Text = "Traditional Method For Books 1M";
            this.traditionalMethodFor1MToolStripMenuItem.Click += new System.EventHandler(this.traditionalMethodFor1MToolStripMenuItem_Click);
            // 
            // traditionalMethodForFilm100KToolStripMenuItem
            // 
            this.traditionalMethodForFilm100KToolStripMenuItem.Name = "traditionalMethodForFilm100KToolStripMenuItem";
            this.traditionalMethodForFilm100KToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.traditionalMethodForFilm100KToolStripMenuItem.Text = "Traditional Method for Film 100K";
            this.traditionalMethodForFilm100KToolStripMenuItem.Click += new System.EventHandler(this.traditionalMethodForFilm100KToolStripMenuItem_Click);
            // 
            // traditionalMethodForFilm1MToolStripMenuItem
            // 
            this.traditionalMethodForFilm1MToolStripMenuItem.Name = "traditionalMethodForFilm1MToolStripMenuItem";
            this.traditionalMethodForFilm1MToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.traditionalMethodForFilm1MToolStripMenuItem.Text = "Traditional Method for Film 1M";
            this.traditionalMethodForFilm1MToolStripMenuItem.Click += new System.EventHandler(this.traditionalMethodForFilm1MToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 27);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageData;
            this.xtraTabControl1.Size = new System.Drawing.Size(763, 388);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageData,
            this.xtraTabPageBindingData,
            this.xtraTabPageAbout});
            // 
            // xtraTabPageData
            // 
            this.xtraTabPageData.Controls.Add(this.gridControl1);
            this.xtraTabPageData.Name = "xtraTabPageData";
            this.xtraTabPageData.Size = new System.Drawing.Size(757, 360);
            this.xtraTabPageData.Text = "Grid Data";
            // 
            // xtraTabPageBindingData
            // 
            this.xtraTabPageBindingData.AutoScroll = true;
            this.xtraTabPageBindingData.Controls.Add(scoreLabel);
            this.xtraTabPageBindingData.Controls.Add(this.scoreSpinEdit);
            this.xtraTabPageBindingData.Controls.Add(timeLabel1);
            this.xtraTabPageBindingData.Controls.Add(this.timeTextEdit);
            this.xtraTabPageBindingData.Controls.Add(productIdLabel);
            this.xtraTabPageBindingData.Controls.Add(this.productIdTextEdit);
            this.xtraTabPageBindingData.Controls.Add(titleLabel);
            this.xtraTabPageBindingData.Controls.Add(this.titleTextEdit);
            this.xtraTabPageBindingData.Controls.Add(priceLabel);
            this.xtraTabPageBindingData.Controls.Add(this.priceTextEdit);
            this.xtraTabPageBindingData.Controls.Add(userIdLabel);
            this.xtraTabPageBindingData.Controls.Add(this.userIdTextEdit);
            this.xtraTabPageBindingData.Controls.Add(profileNameLabel);
            this.xtraTabPageBindingData.Controls.Add(this.profileNameTextEdit);
            this.xtraTabPageBindingData.Controls.Add(helpfulnessLabel);
            this.xtraTabPageBindingData.Controls.Add(this.helpfulnessSpinEdit);
            this.xtraTabPageBindingData.Controls.Add(summaryLabel);
            this.xtraTabPageBindingData.Controls.Add(this.summaryTextEdit);
            this.xtraTabPageBindingData.Controls.Add(textLabel);
            this.xtraTabPageBindingData.Controls.Add(this.textRichTextBox);
            this.xtraTabPageBindingData.Name = "xtraTabPageBindingData";
            this.xtraTabPageBindingData.Size = new System.Drawing.Size(757, 360);
            this.xtraTabPageBindingData.Text = "Binding Data";
            // 
            // scoreSpinEdit
            // 
            this.scoreSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "Score", true));
            this.scoreSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.scoreSpinEdit.Location = new System.Drawing.Point(453, 72);
            this.scoreSpinEdit.Name = "scoreSpinEdit";
            this.scoreSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.scoreSpinEdit.Size = new System.Drawing.Size(46, 20);
            this.scoreSpinEdit.TabIndex = 22;
            // 
            // amazoneBooksBindingSource
            // 
            this.amazoneBooksBindingSource.DataMember = "AmazoneBooks";
            this.amazoneBooksBindingSource.DataSource = this.amazoneDataSetBindingSource;
            this.amazoneBooksBindingSource.PositionChanged += new System.EventHandler(this.amazoneBooksBindingSource_PositionChanged);
            // 
            // amazoneDataSetBindingSource
            // 
            this.amazoneDataSetBindingSource.DataSource = this.amazoneDataSet;
            this.amazoneDataSetBindingSource.Position = 0;
            // 
            // amazoneDataSet
            // 
            this.amazoneDataSet.DataSetName = "AmazoneDataSet";
            this.amazoneDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // timeTextEdit
            // 
            this.timeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "Time", true));
            this.timeTextEdit.Location = new System.Drawing.Point(453, 98);
            this.timeTextEdit.Name = "timeTextEdit";
            this.timeTextEdit.Size = new System.Drawing.Size(100, 20);
            this.timeTextEdit.TabIndex = 21;
            // 
            // productIdTextEdit
            // 
            this.productIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "ProductId", true));
            this.productIdTextEdit.Location = new System.Drawing.Point(122, 16);
            this.productIdTextEdit.Name = "productIdTextEdit";
            this.productIdTextEdit.Size = new System.Drawing.Size(200, 20);
            this.productIdTextEdit.TabIndex = 1;
            // 
            // titleTextEdit
            // 
            this.titleTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "Title", true));
            this.titleTextEdit.Location = new System.Drawing.Point(122, 42);
            this.titleTextEdit.Name = "titleTextEdit";
            this.titleTextEdit.Size = new System.Drawing.Size(200, 20);
            this.titleTextEdit.TabIndex = 3;
            // 
            // priceTextEdit
            // 
            this.priceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "Price", true));
            this.priceTextEdit.Location = new System.Drawing.Point(122, 68);
            this.priceTextEdit.Name = "priceTextEdit";
            this.priceTextEdit.Size = new System.Drawing.Size(200, 20);
            this.priceTextEdit.TabIndex = 5;
            // 
            // userIdTextEdit
            // 
            this.userIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "UserId", true));
            this.userIdTextEdit.Location = new System.Drawing.Point(122, 94);
            this.userIdTextEdit.Name = "userIdTextEdit";
            this.userIdTextEdit.Size = new System.Drawing.Size(200, 20);
            this.userIdTextEdit.TabIndex = 7;
            // 
            // profileNameTextEdit
            // 
            this.profileNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "ProfileName", true));
            this.profileNameTextEdit.Location = new System.Drawing.Point(453, 16);
            this.profileNameTextEdit.Name = "profileNameTextEdit";
            this.profileNameTextEdit.Size = new System.Drawing.Size(200, 20);
            this.profileNameTextEdit.TabIndex = 9;
            // 
            // helpfulnessSpinEdit
            // 
            this.helpfulnessSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "Helpfulness", true));
            this.helpfulnessSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.helpfulnessSpinEdit.Location = new System.Drawing.Point(453, 42);
            this.helpfulnessSpinEdit.Name = "helpfulnessSpinEdit";
            this.helpfulnessSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.helpfulnessSpinEdit.Size = new System.Drawing.Size(46, 20);
            this.helpfulnessSpinEdit.TabIndex = 11;
            // 
            // summaryTextEdit
            // 
            this.summaryTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.amazoneBooksBindingSource, "Summary", true));
            this.summaryTextEdit.Location = new System.Drawing.Point(122, 134);
            this.summaryTextEdit.Name = "summaryTextEdit";
            this.summaryTextEdit.Size = new System.Drawing.Size(200, 20);
            this.summaryTextEdit.TabIndex = 17;
            // 
            // textRichTextBox
            // 
            this.textRichTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.amazoneBooksBindingSource, "Text", true));
            this.textRichTextBox.Location = new System.Drawing.Point(122, 174);
            this.textRichTextBox.Name = "textRichTextBox";
            this.textRichTextBox.Size = new System.Drawing.Size(608, 160);
            this.textRichTextBox.TabIndex = 19;
            this.textRichTextBox.Text = "";
            // 
            // xtraTabPageAbout
            // 
            this.xtraTabPageAbout.Controls.Add(this.richEditControl1);
            this.xtraTabPageAbout.Name = "xtraTabPageAbout";
            this.xtraTabPageAbout.Size = new System.Drawing.Size(757, 360);
            this.xtraTabPageAbout.Text = "About";
            // 
            // richEditControl1
            // 
            this.richEditControl1.AllowDrop = false;
            this.richEditControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richEditControl1.DragDropMode = DevExpress.XtraRichEdit.DragDropMode.Manual;
            this.richEditControl1.Location = new System.Drawing.Point(-1, 0);
            this.richEditControl1.Name = "richEditControl1";
            this.richEditControl1.Options.Behavior.Drop = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.richEditControl1.Size = new System.Drawing.Size(763, 354);
            this.richEditControl1.TabIndex = 0;
            // 
            // amazoneBooksTableAdapter
            // 
            this.amazoneBooksTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AmazoneBooks1MTableAdapter = null;
            this.tableAdapterManager.AmazoneBooksTableAdapter = this.amazoneBooksTableAdapter;
            this.tableAdapterManager.AmazoneMoviesTV1MTableAdapter = null;
            this.tableAdapterManager.AmazoneMoviesTVTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = ConsoleApplication1.AmazoneDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // amazoneMoviesTVBindingSource
            // 
            this.amazoneMoviesTVBindingSource.DataMember = "AmazoneMoviesTV";
            this.amazoneMoviesTVBindingSource.DataSource = this.amazoneDataSetBindingSource;
            this.amazoneMoviesTVBindingSource.PositionChanged += new System.EventHandler(this.amazoneMoviesTVBindingSource_PositionChanged);
            // 
            // amazoneMoviesTVTableAdapter
            // 
            this.amazoneMoviesTVTableAdapter.ClearBeforeFill = true;
            // 
            // viewAmazoneBooksBindingSource
            // 
            this.viewAmazoneBooksBindingSource.DataMember = "View_AmazoneBooks";
            this.viewAmazoneBooksBindingSource.DataSource = this.amazoneDataSetBindingSource;
            this.viewAmazoneBooksBindingSource.PositionChanged += new System.EventHandler(this.viewAmazoneBooksBindingSource_PositionChanged);
            // 
            // view_AmazoneBooksTableAdapter
            // 
            this.view_AmazoneBooksTableAdapter.ClearBeforeFill = true;
            // 
            // viewAmazoneBooksRatingBindingSource
            // 
            this.viewAmazoneBooksRatingBindingSource.DataMember = "View_AmazoneBooksRating";
            this.viewAmazoneBooksRatingBindingSource.DataSource = this.amazoneDataSetBindingSource;
            this.viewAmazoneBooksRatingBindingSource.PositionChanged += new System.EventHandler(this.viewAmazoneBooksRatingBindingSource_PositionChanged);
            // 
            // view_AmazoneBooksRatingTableAdapter
            // 
            this.view_AmazoneBooksRatingTableAdapter.ClearBeforeFill = true;
            // 
            // viewAmazoneMoviesTVBindingSource
            // 
            this.viewAmazoneMoviesTVBindingSource.DataMember = "View_AmazoneMoviesTV";
            this.viewAmazoneMoviesTVBindingSource.DataSource = this.amazoneDataSetBindingSource;
            this.viewAmazoneMoviesTVBindingSource.PositionChanged += new System.EventHandler(this.viewAmazoneMoviesTVBindingSource_PositionChanged);
            // 
            // view_AmazoneMoviesTVTableAdapter
            // 
            this.view_AmazoneMoviesTVTableAdapter.ClearBeforeFill = true;
            // 
            // viewBooksAndMoviesCommonBindingSource
            // 
            this.viewBooksAndMoviesCommonBindingSource.DataMember = "View_BooksAndMoviesCommon";
            this.viewBooksAndMoviesCommonBindingSource.DataSource = this.amazoneDataSetBindingSource;
            this.viewBooksAndMoviesCommonBindingSource.PositionChanged += new System.EventHandler(this.viewBooksAndMoviesCommonBindingSource_PositionChanged);
            // 
            // view_BooksAndMoviesCommonTableAdapter
            // 
            this.view_BooksAndMoviesCommonTableAdapter.ClearBeforeFill = true;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.RelationName = "AmazoneBooks";
            gridLevelNode2.RelationName = "AmazoneBooks1M";
            gridLevelNode3.RelationName = "AmazoneMoviesTV";
            gridLevelNode4.RelationName = "AmazoneMoviesTV1M";
            gridLevelNode5.RelationName = "View_AmazoneBooks";
            gridLevelNode6.RelationName = "View_AmazoneBooks1M";
            gridLevelNode7.RelationName = "View_AmazoneBooksRating";
            gridLevelNode8.RelationName = "View_AmazoneBooksRating1M";
            gridLevelNode9.RelationName = "View_AmazoneMoviesTV";
            gridLevelNode10.RelationName = "View_AmazoneMoviesTV1M";
            gridLevelNode11.RelationName = "View_BooksAndMoviesCommon";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode4,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7,
            gridLevelNode8,
            gridLevelNode9,
            gridLevelNode10,
            gridLevelNode11});
            this.gridControl1.Location = new System.Drawing.Point(-1, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(763, 365);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 413);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageData.ResumeLayout(false);
            this.xtraTabPageBindingData.ResumeLayout(false);
            this.xtraTabPageBindingData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scoreSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amazoneBooksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amazoneDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amazoneDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpfulnessSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summaryTextEdit.Properties)).EndInit();
            this.xtraTabPageAbout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.amazoneMoviesTVBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewAmazoneBooksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewAmazoneBooksRatingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewAmazoneMoviesTVBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewBooksAndMoviesCommonBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem booksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moviesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ratingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem booksUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moviesUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commonUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem experiementalResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem traditionalMethodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem traditionalMethodFor1MToolStripMenuItem;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageData;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAbout;
        private DevExpress.XtraRichEdit.RichEditControl richEditControl1;
        private System.Windows.Forms.ToolStripMenuItem traditionalMethodForFilm100KToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem traditionalMethodForFilm1MToolStripMenuItem;
        private System.Windows.Forms.BindingSource amazoneDataSetBindingSource;
        private AmazoneDataSet amazoneDataSet;
        private System.Windows.Forms.BindingSource amazoneBooksBindingSource;
        private AmazoneDataSetTableAdapters.AmazoneBooksTableAdapter amazoneBooksTableAdapter;
        private AmazoneDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageBindingData;
        private DevExpress.XtraEditors.TextEdit productIdTextEdit;
        private DevExpress.XtraEditors.TextEdit titleTextEdit;
        private DevExpress.XtraEditors.TextEdit priceTextEdit;
        private DevExpress.XtraEditors.TextEdit userIdTextEdit;
        private DevExpress.XtraEditors.TextEdit profileNameTextEdit;
        private DevExpress.XtraEditors.SpinEdit helpfulnessSpinEdit;
        private DevExpress.XtraEditors.TextEdit summaryTextEdit;
        private System.Windows.Forms.RichTextBox textRichTextBox;
        private DevExpress.XtraEditors.SpinEdit scoreSpinEdit;
        private DevExpress.XtraEditors.TextEdit timeTextEdit;
        private System.Windows.Forms.BindingSource amazoneMoviesTVBindingSource;
        private AmazoneDataSetTableAdapters.AmazoneMoviesTVTableAdapter amazoneMoviesTVTableAdapter;
        private System.Windows.Forms.BindingSource viewAmazoneBooksBindingSource;
        private AmazoneDataSetTableAdapters.View_AmazoneBooksTableAdapter view_AmazoneBooksTableAdapter;
        private System.Windows.Forms.BindingSource viewAmazoneBooksRatingBindingSource;
        private AmazoneDataSetTableAdapters.View_AmazoneBooksRatingTableAdapter view_AmazoneBooksRatingTableAdapter;
        private System.Windows.Forms.BindingSource viewAmazoneMoviesTVBindingSource;
        private AmazoneDataSetTableAdapters.View_AmazoneMoviesTVTableAdapter view_AmazoneMoviesTVTableAdapter;
        private System.Windows.Forms.BindingSource viewBooksAndMoviesCommonBindingSource;
        private AmazoneDataSetTableAdapters.View_BooksAndMoviesCommonTableAdapter view_BooksAndMoviesCommonTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}