﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ConsoleApplication1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ratingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 0;

            gridControl1.DataSource = viewAmazoneBooksRatingBindingSource;
            gridControl1.RefreshDataSource();
            gridView1.PopulateColumns();
            gridControl1.Refresh();
        }

        private void moviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 0;

            gridControl1.DataSource = amazoneMoviesTVBindingSource;
            gridControl1.RefreshDataSource();
            gridView1.PopulateColumns();
            gridControl1.Refresh();
        }

        private void traditionalMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 2;
            richEditControl1.Text = "Traditional Method on 100K amazone books" + RecommendHandler.PredictOnSQL();
        }

        private void booksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 0;

            gridControl1.DataSource = amazoneBooksBindingSource;
            gridControl1.RefreshDataSource();
            gridView1.PopulateColumns();
            gridControl1.Refresh();
        }

        private void traditionalMethodFor1MToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 2;
            richEditControl1.Text = "Traditional Method on 1M amazone books" + RecommendHandler.PredictOn1MSQL();
        }

        private void booksUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 0;

            gridControl1.DataSource = viewAmazoneBooksBindingSource;
            gridControl1.RefreshDataSource();
            gridView1.PopulateColumns();
            gridControl1.Refresh();
        }

        private void moviesUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 0;

            gridControl1.DataSource = viewAmazoneMoviesTVBindingSource;
            gridControl1.RefreshDataSource();
            gridView1.PopulateColumns();
            gridControl1.Refresh();
        }

        private void commonUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 0;

            gridControl1.DataSource = viewBooksAndMoviesCommonBindingSource;
            gridControl1.RefreshDataSource();
            gridView1.PopulateColumns();
            gridControl1.Refresh();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 2;
            richEditControl1.Text = "Demo cac thuat toan tu van cho Sach va Phim.";
        }

        private void traditionalMethodForFilm100KToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 2;
            richEditControl1.Text = "Traditional Method on 100K amazone movies" + RecommendHandler.PredictOnSQL();
        }

        private void traditionalMethodForFilm1MToolStripMenuItem_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 2;
            richEditControl1.Text = "Traditional Method on 1M amazone movies" + RecommendHandler.PredictOn1MSQL();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'amazoneDataSet.View_BooksAndMoviesCommon' table. You can move, or remove it, as needed.
            this.view_BooksAndMoviesCommonTableAdapter.Fill(this.amazoneDataSet.View_BooksAndMoviesCommon);
            // TODO: This line of code loads data into the 'amazoneDataSet.View_AmazoneMoviesTV' table. You can move, or remove it, as needed.
            this.view_AmazoneMoviesTVTableAdapter.Fill(this.amazoneDataSet.View_AmazoneMoviesTV);
            // TODO: This line of code loads data into the 'amazoneDataSet.View_AmazoneBooksRating' table. You can move, or remove it, as needed.
            this.view_AmazoneBooksRatingTableAdapter.Fill(this.amazoneDataSet.View_AmazoneBooksRating);
            // TODO: This line of code loads data into the 'amazoneDataSet.View_AmazoneBooks' table. You can move, or remove it, as needed.
            this.view_AmazoneBooksTableAdapter.Fill(this.amazoneDataSet.View_AmazoneBooks);
            // TODO: This line of code loads data into the 'amazoneDataSet.AmazoneMoviesTV' table. You can move, or remove it, as needed.
            this.amazoneMoviesTVTableAdapter.Fill(this.amazoneDataSet.AmazoneMoviesTV);
            // TODO: This line of code loads data into the 'amazoneDataSet.AmazoneBooks' table. You can move, or remove it, as needed.
            this.amazoneBooksTableAdapter.Fill(this.amazoneDataSet.AmazoneBooks);
            xtraTabControl1.SelectedTabPageIndex = 0;
        }

        private void amazoneBooksBindingSource_PositionChanged(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 1;
            DataRowView selectedRow = (DataRowView)amazoneBooksBindingSource.Current;

            if (selectedRow != null)
            {
                ClearText();
                productIdTextEdit.Text = selectedRow.Row["ProductId"].ToString();
                titleTextEdit.Text = selectedRow.Row["Title"].ToString();
                textRichTextBox.Text = selectedRow.Row["Text"].ToString();
                profileNameTextEdit.Text = selectedRow.Row["ProfileName"].ToString();
                timeTextEdit.Text = selectedRow.Row["Time"].ToString();
                priceTextEdit.Text = selectedRow.Row["Price"].ToString();
                helpfulnessSpinEdit.Value = Convert.ToDecimal(selectedRow.Row["Helpfulness"].ToString());
                scoreSpinEdit.Value = Convert.ToDecimal(selectedRow.Row["Score"].ToString());
                userIdTextEdit.Text = selectedRow.Row["UserId"].ToString();
                summaryTextEdit.Text = selectedRow.Row["Summary"].ToString();
            }
        }

        private void amazoneMoviesTVBindingSource_PositionChanged(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 1;
            DataRowView selectedRow = (DataRowView)amazoneMoviesTVBindingSource.Current;

            if (selectedRow != null)
            {
                ClearText();
                productIdTextEdit.Text = selectedRow.Row["ProductId"].ToString();
                titleTextEdit.Text = selectedRow.Row["Title"].ToString();
                textRichTextBox.Text = selectedRow.Row["Text"].ToString();
                profileNameTextEdit.Text = selectedRow.Row["ProfileName"].ToString();
                timeTextEdit.Text = selectedRow.Row["Time"].ToString();
                priceTextEdit.Text = selectedRow.Row["Price"].ToString();
                helpfulnessSpinEdit.Value = Convert.ToDecimal(selectedRow.Row["Helpfulness"].ToString());
                scoreSpinEdit.Value = Convert.ToDecimal(selectedRow.Row["Score"].ToString());
                userIdTextEdit.Text = selectedRow.Row["UserId"].ToString();
                summaryTextEdit.Text = selectedRow.Row["Summary"].ToString();
            }
        }

        private void viewAmazoneBooksBindingSource_PositionChanged(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 1;
            DataRowView selectedRow = (DataRowView)viewAmazoneBooksBindingSource.Current;

            if (selectedRow != null)
            {
                ClearText();
                profileNameTextEdit.Text = selectedRow.Row["ProfileName"].ToString();
                userIdTextEdit.Text = selectedRow.Row["UserId"].ToString();
            }
        }

        private void viewAmazoneBooksRatingBindingSource_PositionChanged(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 1;
            DataRowView selectedRow = (DataRowView)viewAmazoneBooksRatingBindingSource.Current;

            if (selectedRow != null)
            {
                ClearText();
                productIdTextEdit.Text = selectedRow.Row["ProductId"].ToString();
                timeTextEdit.Text = selectedRow.Row["timestamp"].ToString();
                scoreSpinEdit.Value = Convert.ToDecimal(selectedRow.Row["rating"].ToString());
                userIdTextEdit.Text = selectedRow.Row["UserId"].ToString();
            }
        }

        private void viewAmazoneMoviesTVBindingSource_PositionChanged(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 1;
            DataRowView selectedRow = (DataRowView)viewAmazoneMoviesTVBindingSource.Current;

            if (selectedRow != null)
            {
                ClearText();
                profileNameTextEdit.Text = selectedRow.Row["ProfileName"].ToString();
                userIdTextEdit.Text = selectedRow.Row["UserId"].ToString();
            }
        }

        private void viewBooksAndMoviesCommonBindingSource_PositionChanged(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = 1;
            DataRowView selectedRow = (DataRowView)viewBooksAndMoviesCommonBindingSource.Current;

            if (selectedRow != null)
            {
                ClearText();
                profileNameTextEdit.Text = selectedRow.Row["ProfileName"].ToString();
                userIdTextEdit.Text = selectedRow.Row["UserId"].ToString();
            }
        }

        private void ClearText()
        {
            productIdTextEdit.ResetText();
            titleTextEdit.ResetText();
            textRichTextBox.ResetText();
            profileNameTextEdit.ResetText();
            priceTextEdit.ResetText();
            helpfulnessSpinEdit.ResetText();
            scoreSpinEdit.ResetText();
            userIdTextEdit.ResetText();
            summaryTextEdit.ResetText();
            timeTextEdit.ResetText();
        }
    }
}
